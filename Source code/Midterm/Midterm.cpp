﻿// Midterm.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Midterm.h"
#include <windowsx.h>
#include <WinUser.h>
#include <CommCtrl.h>
#include <ObjIdl.h>
#include <gdiplus.h>
#pragma comment(lib, "Gdiplus.lib")
using namespace Gdiplus;

#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment(lib, "ComCtl32.lib")
#include <vector>
using namespace std;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_MIDTERM, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_MIDTERM));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_BTNFACE+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_MIDTERM);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, TEXT("QUẢN LÝ CHI TIÊU"), WS_OVERLAPPEDWINDOW,
      200, 70, 800, 550, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//

HWND btnThem;
HWND txtNoiDung;
HWND txtTien;
HWND cbLoai;
HWND cbDate;
HWND listview;
HWND txtTong;
const WCHAR *items[] = { TEXT("Ăn uống"), TEXT("Di chuyển"), TEXT("Nhà cửa"), TEXT("Xe cộ"), TEXT("Nhu yếu phẩm"), TEXT("Dịch vụ") };

GdiplusStartupInput gdiplusStartupInput;
ULONG_PTR           gdiplusToken;

struct DATA
{
	WCHAR* Loai;
	WCHAR* noiDung;
	WCHAR* Tien;
	WCHAR* ngayThang;
};

vector<DATA>input;
FILE *f;
FILE *rf;
int Tong = 0;


void Load()
{
	DATA s;
	WCHAR* buffer;
	rf = _wfopen(L"Save.txt", L"rb");
	if (rf == NULL) return;
	while (!feof(rf))
	{
		int i = 0;
		buffer = new WCHAR[20];
		while (!feof(rf))
		{
			fread(&buffer[i], 2, 1, rf);
			if (buffer[i] == L'*')
			{
				buffer[i] = 0;
				s.Loai = buffer;
				break;
			}
			i++;
		}
		int flag = 0;
		for (int j = 0; j < 6; j++)
		{
			if (wcscmp(buffer, items[j]) == 0)
			{
				flag = 1;
				break;
			}
		}
		if (flag == 0) return;
		buffer = new WCHAR[100];
		i = 0;
		while (!feof(rf))
		{
			fread(&buffer[i], 2, 1,rf);
			if (buffer[i] == L'*')
			{
				buffer[i] = 0;
				s.noiDung = buffer;
				break;
			}
			i++;
		}
		buffer = new WCHAR[10];
		i = 0;
		while (!feof(rf))
		{
			fread(&buffer[i], 2, 1, rf);
			if (buffer[i] == L'*')
			{
				buffer[i] = 0;
				s.ngayThang = buffer;
				break;
			}
			i++;
		}
		buffer = new WCHAR[11];
		i = 0;
		while (!feof(rf))
		{
			fread(&buffer[i], 2, 1, rf);
			if (buffer[i] == L'*')
			{
				buffer[i] = 0;
				s.Tien = buffer;
				break;
			}
			i++;
		}
		input.push_back(s);
	}
	

}

void CreateList(WCHAR* a, WCHAR* b, WCHAR* c, WCHAR* d)
{
	LVITEM lv;
	lv.mask = LVIF_TEXT;
	lv.iItem = 0;
	lv.iSubItem = 0;
	lv.pszText = a;
	ListView_InsertItem(listview, &lv);

	ListView_SetItemText(listview, 0, 1, b);
	ListView_SetItemText(listview, 0, 2, c);
	ListView_SetItemText(listview, 0, 3, d);
}

void LoadListView()
{

	for (int i= 0; i < input.size(); i++)
	{
		CreateList(input[i].Loai, input[i].noiDung, input[i].Tien, input[i].ngayThang);
	}
}

void tinhTien()
{
	Tong = 0;
	int x;
	for (int i = 0; i < input.size(); i++)
	{
		x = _wtoi(input[i].Tien);
		Tong += x;
	}
	WCHAR* sum = new WCHAR[20];
	sum = _itow(Tong, sum, 10);
	SetWindowText(txtTong, sum);
	delete sum;
}

float phanTram(WCHAR* x)
{
	float tong = 0;
	if (input.size() == 0)
	{
		return 0;
	}
	else
	{
		for (int i = 0; i < input.size(); i++)
		{
			if (wcscmp(input[i].Loai, x) == 0)
			{
				float x = _wtoi(input[i].Tien);
				tong += x;
			}
		}
		float pt = (tong / Tong) * 100;
		return pt;
	}
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	
	int wmEvent;
	int wmId = LOWORD(wParam);
	PAINTSTRUCT ps;
	HDC hdc;
	switch (message)
	{
	case WM_CREATE:
	{
					  f = _wfopen(L"Save.txt", L"ab");

					  INITCOMMONCONTROLSEX icc;
					  icc.dwSize = sizeof(icc);
					  icc.dwICC = ICC_WIN95_CLASSES;
					  InitCommonControlsEx(&icc);


					  // Lấy font hệ thống
					  LOGFONT lf;
					  GetObject(GetStockObject(DEFAULT_GUI_FONT), sizeof(LOGFONT), &lf);
					  HFONT hFont = CreateFont(20, 7,
						  lf.lfEscapement, lf.lfOrientation, lf.lfWeight,
						  lf.lfItalic, lf.lfUnderline, lf.lfStrikeOut, lf.lfCharSet,
						  lf.lfOutPrecision, lf.lfClipPrecision, lf.lfQuality,
						  lf.lfPitchAndFamily, TEXT("Segoe UI"));

					  HWND hwnd = CreateWindowEx(0, L"BUTTON", L"Thêm chi tiêu", BS_GROUPBOX | WS_CHILD | WS_VISIBLE, 10, 10, 370, 220, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Loại chi tiêu:", WS_CHILD | WS_VISIBLE | SS_LEFT, 30, 40, 80, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Nội dung:", WS_CHILD | WS_VISIBLE | SS_LEFT, 30, 80, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Số tiền:", WS_CHILD | WS_VISIBLE | SS_LEFT, 30, 120, 50, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"VND", WS_CHILD | WS_VISIBLE | SS_LEFT, 300, 120, 30, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Thời điểm:", WS_CHILD | WS_VISIBLE | SS_LEFT, 30, 160, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  btnThem = CreateWindowEx(0, L"BUTTON", L"Thêm", WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON | WS_BORDER, 170, 198, 50, 20, hWnd, (HMENU)IDC_THEM, hInst, NULL);
					  SendMessage(btnThem, WM_SETFONT, WPARAM(hFont), TRUE);

					  txtNoiDung = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT | ES_AUTOHSCROLL, 140, 75, 150, 25, hWnd, (HMENU)IDC_NOIDUNG, hInst, NULL);
					  SetWindowFont(txtNoiDung, hFont, TRUE);

					  txtTien = CreateWindowEx(0, L"EDIT", L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_NUMBER | ES_LEFT | ES_AUTOHSCROLL, 140, 115, 150, 25, hWnd, (HMENU)IDC_TIEN, hInst, NULL);
					  SetWindowFont(txtTien, hFont, TRUE);

					  cbLoai = CreateWindowEx(0, L"COMBOBOX", L"", WS_CHILD | WS_VISIBLE | CBS_DROPDOWNLIST, 140, 35, 150, 25, hWnd, (HMENU)IDLOAI, hInst, NULL);
					  SendMessage(cbLoai, 0, WPARAM(hFont), TRUE);

					  SendMessage(cbLoai, CB_ADDSTRING, NULL, (LPARAM)L"Ăn uống");
					  SendMessage(cbLoai, CB_ADDSTRING, NULL, (LPARAM)L"Di chuyển");
					  SendMessage(cbLoai, CB_ADDSTRING, NULL, (LPARAM)L"Nhà cửa");
					  SendMessage(cbLoai, CB_ADDSTRING, NULL, (LPARAM)L"Xe cộ");
					  SendMessage(cbLoai, CB_ADDSTRING, NULL, (LPARAM)L"Nhu yếu phẩm");
					  SendMessage(cbLoai, CB_ADDSTRING, NULL, (LPARAM)L"Dịch vụ");
					  SetWindowFont(cbLoai, hFont, TRUE);
					  SetWindowFont(hWnd, hFont, TRUE);

					  cbDate = CreateWindowEx(0, DATETIMEPICK_CLASS, L"", WS_CHILD | WS_VISIBLE, 140, 160, 150, 25, hWnd, (HMENU)ID_CB_DATE, hInst, NULL);
					  SendMessage(cbDate, 0, WPARAM(hFont), TRUE);
					  SetWindowFont(cbDate, hFont, TRUE);

					  //------------------------
					  hwnd = CreateWindowEx(0, L"BUTTON", L"Danh sách chi tiêu", BS_GROUPBOX | WS_CHILD | WS_VISIBLE, 10, 230, 370, 255, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  listview = CreateWindowEx(NULL, WC_LISTVIEWW, L"",
						  WS_CHILD | WS_VISIBLE | WS_HSCROLL | WS_VSCROLL | ES_AUTOHSCROLL | ES_AUTOVSCROLL | LVS_REPORT | WS_BORDER | LVS_ALIGNTOP,
						  15, 250, 360, 230, hWnd, (HMENU)IDC_LISTVIEW, hInst, NULL);
					  SetWindowFont(listview, hFont, TRUE);

					  // Tạo ra 4 cột
					  LVCOLUMN lvCol1;
					  lvCol1.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
					  lvCol1.fmt = LVCFMT_LEFT;
					  lvCol1.pszText = L"Loại";
					  lvCol1.cx = 91;
					  ListView_InsertColumn(listview, 0, &lvCol1);

					  LVCOLUMN lvCol2;
					  lvCol2.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
					  lvCol2.fmt = LVCFMT_LEFT;
					  lvCol2.pszText = L"Nội dung";
					  lvCol2.cx = 92;
					  ListView_InsertColumn(listview, 1, &lvCol2);

					  LVCOLUMN lvCol3;
					  lvCol3.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
					  lvCol3.fmt = LVCFMT_LEFT;
					  lvCol3.pszText = L"Số tiền";
					  lvCol3.cx = 91;
					  ListView_InsertColumn(listview, 2, &lvCol3);

					  LVCOLUMN lvCol4;
					  lvCol4.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;
					  lvCol4.fmt = LVCFMT_LEFT;
					  lvCol4.pszText = L"Thời điểm";
					  lvCol4.cx = 90;
					  ListView_InsertColumn(listview, 3, &lvCol4);

					  //----------------
					  hwnd = CreateWindowEx(0, L"BUTTON", L"Biểu đồ", BS_GROUPBOX | WS_CHILD | WS_VISIBLE, 390, 10, 383, 400, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);


					  //--------------------
					  hwnd = CreateWindowEx(0, L"BUTTON", L"", BS_GROUPBOX | WS_CHILD | WS_VISIBLE, 390, 410, 383, 75, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Tổng chi:", WS_CHILD | WS_VISIBLE | SS_LEFT, 450, 430, 80, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  txtTong = CreateWindowEx(0, L"STATIC", L"", WS_CHILD | WS_VISIBLE | SS_LEFT, 550, 445, 80, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  //Text chú thích
					  //TEXT("Di chuyển"), TEXT("Nhà cửa"), TEXT("Xe cộ"), TEXT("Nhu yếu phẩm"), TEXT("Dịch vụ") };
					  hwnd = CreateWindowEx(0, L"STATIC", L"Ăn uống", WS_CHILD | WS_VISIBLE | SS_LEFT, 480, 270, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Di chuyển", WS_CHILD | WS_VISIBLE | SS_LEFT, 480, 310, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Nhà cửa", WS_CHILD | WS_VISIBLE | SS_LEFT, 480, 350, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Xe cộ", WS_CHILD | WS_VISIBLE | SS_LEFT, 660, 270, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Nhu yếu phẩm", WS_CHILD | WS_VISIBLE | SS_LEFT, 660, 310, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"Dịch vụ", WS_CHILD | WS_VISIBLE | SS_LEFT, 660, 350, 70, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  hwnd = CreateWindowEx(0, L"STATIC", L"VND", WS_CHILD | WS_VISIBLE | SS_LEFT, 640, 440, 30, 20, hWnd, NULL, hInst, NULL);
					  SendMessage(hwnd, WM_SETFONT, WPARAM(hFont), TRUE);

					  Load();
					  LoadListView();
					  tinhTien();
					  GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);




					  break;
	}
	case WM_COMMAND:
	{
					   wmEvent = HIWORD(wParam);


					   // Parse the menu selections:

					   switch (wmId)
					   {
					   case IDC_THEM:
					   {

										int size = 0;
										WCHAR* buffer;
										DATA temp;

										size = GetWindowTextLength(cbDate);
										buffer = new WCHAR[size + 1];
										GetWindowText(cbDate, buffer, size + 1);
										temp.ngayThang = buffer;

										size = GetWindowTextLength(txtNoiDung);
										buffer = new WCHAR[size + 1];
										GetWindowText(txtNoiDung, buffer, size + 1);
										temp.noiDung = buffer;


										size = GetWindowTextLength(txtTien);
										buffer = new WCHAR[size + 1];
										GetWindowText(txtTien, buffer, size + 1);
										temp.Tien = buffer;

										int i = ComboBox_GetCurSel(cbLoai);
										if (i >= 0)
										{
											size = wcslen(items[i]);
											buffer = new WCHAR[size + 1];
											wcscpy(buffer, items[i]);
											temp.Loai = buffer;
										}

										fwrite(temp.Loai, 2, wcslen(temp.Loai), f);
										fwrite(L"*", 2, 1, f);
										fwrite(temp.noiDung, 2, wcslen(temp.noiDung), f);
										fwrite(L"*", 2, 1, f);
										fwrite(temp.ngayThang, 2, wcslen(temp.ngayThang), f);
										fwrite(L"*", 2, 1, f);
										fwrite(temp.Tien, 2, wcslen(temp.Tien), f);
										fwrite(L"*", 2, 1, f);
										input.push_back(temp);
										CreateList(temp.Loai, temp.noiDung, temp.Tien, temp.ngayThang);
										tinhTien();

										SetWindowText(txtNoiDung, NULL);
										SetWindowText(txtTien, NULL);
										InvalidateRect(hWnd, NULL, TRUE);
										break;
					   }

					   case IDM_ABOUT:
					   {
										 DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
										 break;
					   }
					   case IDM_EXIT:
						   DestroyWindow(hWnd);
						   fclose(f);
						   break;
					   default:
						   return DefWindowProc(hWnd, message, wParam, lParam);
					   }

					   break;
	}

	case WM_PAINT:
	{
					 hdc = BeginPaint(hWnd, &ps);
					 Graphics graphics(hdc);

					 //color
					 SolidBrush brush1(Color(128, 128, 255));
					 SolidBrush brush2(Color(255, 128, 128));
					 SolidBrush brush3(Color(0, 255, 64));
					 SolidBrush brush4(Color(128, 0, 255));
					 SolidBrush brush5(Color(255, 128, 192));
					 SolidBrush brush6(Color(255, 128, 64));
					 SolidBrush brushnull(Color(192, 192, 192));
					 SolidBrush* mangmau[6] = { &brush1, &brush2, &brush3, &brush4, &brush5, &brush6 };

					 //Vẽ chú thích
					 graphics.FillPie(mangmau[0], Rect(390, 240, 70, 70), 0, 30);
					 graphics.FillPie(mangmau[1], Rect(390, 280, 70, 70), 0, 30);
					 graphics.FillPie(mangmau[2], Rect(390, 320, 70, 70), 0, 30);
					 graphics.FillPie(mangmau[3], Rect(570, 240, 70, 70), 0, 30);
					 graphics.FillPie(mangmau[4], Rect(570, 280, 70, 70), 0, 30);
					 graphics.FillPie(mangmau[5], Rect(570, 320, 70, 70), 0, 30);
					 
					 //Vẽ biểu đồ
					 if (phanTram(L"Ăn uống") == 0 && phanTram(L"Di chuyển") == 0 && phanTram(L"Nhà cửa") == 0 && phanTram(L"Xe cộ") == 0 && 
						 phanTram(L"Nhu yếu phẩm") == 0 && phanTram(L"Dịch vụ") == 0 )
						 graphics.FillPie(&brushnull, Rect(490, 60, 180, 180), 0, 360);
					 else
					 {
						 graphics.FillPie(mangmau[0], Rect(490, 60, 180, 180), 0, phanTram(L"Ăn uống")*3.6);
						 graphics.FillPie(mangmau[1], Rect(490, 60, 180, 180), phanTram(L"Ăn uống")*3.6, phanTram(L"Di chuyển")*3.6);
						 graphics.FillPie(mangmau[2], Rect(490, 60, 180, 180), phanTram(L"Ăn uống")*3.6 + phanTram(L"Di chuyển")*3.6, phanTram(L"Nhà cửa")*3.6);
						 graphics.FillPie(mangmau[3], Rect(490, 60, 180, 180), phanTram(L"Ăn uống")*3.6 + phanTram(L"Di chuyển")*3.6 + phanTram(L"Nhà cửa")*3.6, phanTram(L"Xe cộ")*3.6);
						 graphics.FillPie(mangmau[4], Rect(490, 60, 180, 180), phanTram(L"Ăn uống")*3.6 + phanTram(L"Di chuyển")*3.6 + phanTram(L"Nhà cửa")*3.6 + phanTram(L"Xe cộ")*3.6, phanTram(L"Nhu yếu phẩm")*3.6);
						 graphics.FillPie(mangmau[5], Rect(490, 60, 180, 180), phanTram(L"Ăn uống")*3.6 + phanTram(L"Di chuyển")*3.6 + phanTram(L"Nhà cửa")*3.6 + phanTram(L"Xe cộ")*3.6 + phanTram(L"Nhu yếu phẩm")*3.6, phanTram(L"Dịch vụ")*3.6);
					 }

					 // TODO: Add any drawing code here...
					 EndPaint(hWnd, &ps);
					 break;
	}
	case WM_DESTROY:
		GdiplusShutdown(gdiplusToken);
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}


