//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Midterm.rc
//
#define IDC_MYICON                      2
#define IDD_MIDTERM_DIALOG              102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define ID_CB_DATE                      106
#define IDI_MIDTERM                     107
#define IDC_LISTVIEW                    107
#define IDI_SMALL                       108
#define IDC_NOIDUNG                     108
#define IDC_MIDTERM                     109
#define IDC_TIEN                        110
#define IDLOAI                          111
#define IDC_THEM                        112
#define IDR_MAINFRAME                   128
#define IDB_BITMAP1                     129
#define IDI_ICON1                       130
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        131
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
