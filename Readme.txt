Họ tên: Trần Thị Nhã
MSSV: 1512364
Email: trannha132@gmail.com

1. Các chức năng làm được:
- Thêm vào danh sách một mục chi, gồm 4 thành phần: Loại chi tiêu, nội dung chi, số tiền, thời điểm.
- Có chức năng tính tổng chi.
- Xem lại danh sách các mục chi của mình, lưu và nạp vào tập tin text.
- Khi chương trình chạy lên tự động nạp danh sách chi tiêu từ tập tin text lên và hiển thị.
- Khi chương trình thoát thì tự động lưu danh sách mới vào tập tin text.
- Tập tin lưu trữ các khoản chi tiêu hỗ trợ lưu và nạp Tiếng Việt
- Vẽ biểu đồ hình tròn (pie chart) để biểu thị tỉ lệ chi tiêu

2. Link Youtube: https://youtu.be/Gkc_-BP6TpA

3. Link Bitbucket: https://bitbucket.org/TranThiNha/myproject-midterm